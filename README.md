# Docker

Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, 

proporcionando una capa adicional de abstracción y automatización de Virtualización a nivel de sistema operativo 

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583203130/contenedores_mlbsqg.png)

Características

- Autogestión de los contenedores.
- fiabilidad.
- Aplicaciones libres de las dependencias instaladas en el sistema anfitrión.
- Capacidad para desplegar multitud de contenedores en un mismo equipo físico.
- Capacidad para ejecutar una amplia gama de aplicaciones
- Compatibilidad Multi-Sistema, podremos desplegar nuestros contenedores en multitud de plataformas.
- Podremos compartir nuestros contenedores para aumentar los repositorios de Docker

### Maquinas virtuales vs contenedores

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583204892/virtualmachinevscontenedore_xsti3l.png)

El proceso de instalación de Docker en Windows 10, es bastante simple, descargarlo desde aquí

https://hub.docker.com/editions/community/docker-ce-desktop-windows

### contenedores vs imagenes

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583207384/contenedoresvsimagen_xrdbeg.png)

Imágenes:
- Plantilla de solo lectura vacía on con una aplicación preinstalada para la creación de contenedores
- Creadas por nosotros o por la comunidad Docker hub

Contenedores:
- Contiene lo necesario para ejecutar nuestras aplicaciones 
- Basados en una o más imágenes

### Docker hub

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583207857/dockerhub_okwr6z.png)

### Visual code

* [Visual code] - Descargar visual code
* [.Net Core SDK] - Descargar .Net Core SDK

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583295135/visualcode_wzbknp.png)

### .Net Core SDK

* [.Net Core SDK] - Descargar .Net Core SDK

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1583295805/netcore_y7idjb.png)

````bash
    dotnet --version
````


[Visual code]: <https://code.visualstudio.com/download>
[.Net Core SDK]: <https://dotnet.microsoft.com/download>

### Comandos base

````bash
    dotnet new console
    dotnet build
    dotnet run
````

### Gestionar el estado de los contenedores

````bash
    docker run hello-world
    docker ps -a
````

#### Renombrar un contenedor

````bash
    docker rename nombreactual nombrenuevo
````

#### Asignar un nombre

````bash
    docker run --name nombre imagen
````

#### Eliminar un contenedor

````bash
    docker rm -f idcontenedor
    docker rm -f $(docker ps -a -q)
````

#### Inspecionar un contenedor

````bash
    docker inspect idcontenedor
````

#### Modo interactivo

````bash
    docker run ubuntu
    docker run -it ubuntu
    docker exec -it id bash
    exit
    docker kill nombrecontenedor
    docker rm -f nombrecontenedor
    docker run ubuntu tail -f /dev/null
````

#### Exponiendo contenedores al mundo exterior

````bash
    docker run --detach
    docker run -d --name server nginx
    docker run -d --name server -p 80:80 nginx
````

#### Mongodb con Docker

````bash
    docker run -d --name db mongo
    docker logs db
    docker exec -it db bash
    mongo
    use prueba
    db.usuario.insert({"nombre":"andres"}
    db.usuario.find()
    exit
    cd d:
    mkdir mongodata
    pwd
    /d
    docker volume ls
    docker volume prune
    docker volume create dbdata
    docker run -d --name db5 -v dbdata:/data/db mongo
````

#### Imagenes

````bash
    docker pull redis
    docker image ls
````

#### Dockerfile

````bash
    FROM ubuntu

    RUN touch /usr/src/hola
    docker build -t ubuntu:afn .
    docker image ls
    docker run -it ubuntu:afn 
    ls /usr/src
````

#### Subir imagenes

````bash
    docker push ubuntu:afn
    docker rename id afnarqui/ubuntu:afn
    docker tag ubuntu:afn afnarqui/ubuntu:afn
    docker push afnarqui/ubuntu:afn
````

#### Sistema de capas

````bash
    docker history ubuntu:afn
    dive ubuntu:afn
    Ctrl u
````

#### Desarrollo de aplicaciones

````bash
    demo
    docker build -t nodedemo .
    docker run--rm -p 3000:3000 -v ruta nodedemo
````

#### Networking

````bash
    docker network ls
    docker network create --attachable afn
    docker network ls
    Docker run -d --name db mongo
    Docker network connect afn db
    Docker network inspect
    Docker run -d --name app -p  3000:3000 --env MONGO_URL=mongodb://db:/27017/prueba node:afn
    Docker network connect afn app
````

#### Docker-compose

````yml
    Version: "3"

    Services:
    App:
    Image: miimagen
    Environment:
        MONGO_URL: "mongodb://db:27017/test
    Depends_on:
        - db
    Ports:
        - "3000-3010:3000"

    db:
        Image:mongo
````

````bash
Docker- compose up

docker-compose up -d
docker-compose ps
Docker-compose exec app bash

Docker- compose down


Build: . A cambio de image
Volumes:
  -  .:/usr/src
 - /usr/src/node_modules

Docker-compose up -d
Docker-compose scale app=4
````

### Docker in Docker


![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1584074204/docker_in_docker_qsipj1.jpg)

#### Comunicando docker en la maquina

````bash
docker run --name datos -v c:/users/afnarqui/proyectos:/c busybox
docker run -i -t --volumes-from datos -v c:/users/afnarqui/proyectos:/c --name dev -p 4001:4001 node bash
````

### Generando nuestras propias imagenes
````bash
docker commit -m "instalo vim en una imagen nodejs" dev
docker tag id nodevim
docker image save --output nodevim nodevim
docker rmi -f nodevim
docker image load --input nodevim
docker run --name dev2 -it --volumes-from datos -v c:/users/afnarqui/proyectos:/c nodevim bash
````

### Docker in Docker

````bash

docker rmi -f $(docker images -q)
docker run --name docker --privileged -t -i -e LOG=file jpetazzo/dind 
docker commit -m "docker in docker" id
docker tag id dockerafn
docker run --name datos -v c:/users/afnarqui/proyectos:/c busybox
docker run --name dev --volumes-from datos -v c:/users/afnarqui/proyectos:/c --privileged -t -i -e LOG=file dockerafn
docker start dev

FROM ubuntu:latest

ENTRYPOINT echo "en docker"

docker image build --tag ejemplo .

docker build -t echo .
docket tag id afnarqui/echo
docker push afnarqui/echo
docker login
docker run --name afn afnarqui/echo
docker exec -it dev bash
service docker start
docker stop dev

````

### Generando copias de las imagenes

````bash
docker commit -m "docker in docker con archivos y vim" devdocker tag 386de014cab0 dev
docker tag idimagen dev
docker save dev > dev.zip
docker rm -f $(docker ps -a -q)
docker rmi -f $(docker images -q)
docker load -i dev.zip
docker run --name datos -v c:/users/afnarqui/proyectos:/c busybox
docker run --name dev --volumes-from datos -v c:/users/afnarqui/proyectos:/c --privileged -t -i -e LOG=file dockerafn
````

### Portainer

![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1584079677/portainer_e6i2lb.png)
````bash

docker volume create portainer_data

docker-compose.yml
version: '2'

services:
  portainer:
    image: portainer/portainer
    ports:
      - "9500:9000"
    command: -H unix:///var/run/docker.sock
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data

volumes:
  portainer_data:
  
  docker compose up -d
  http://localhost:9500/
````












