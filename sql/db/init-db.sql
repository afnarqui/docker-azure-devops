CREATE DATABASE prueba;
GO

USE prueba;

CREATE TABLE datos (
  Id INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
  titulo NVARCHAR(50),
  detalle NVARCHAR(200),
  [Date] DATETIMEOFFSET,
  fechaCreacion DATETIMEOFFSET NOT NULL, 
  fechaActualizacion DATETIMEOFFSET NOT NULL
);

INSERT INTO datos (titulo, detalle, [Date], fechaCreacion, fechaActualizacion) VALUES
(N'Docker', N'Introducción a Docker con sql', '2020-03-04', GETDATE(), GETDATE()),
(N'otro', N'introducción Docker ', '2020-03-05', GETDATE(), GETDATE()),
(N'Docker en Windows', N'producción', '2020-03-03', GETDATE(), GETDATE());

SELECT * FROM prueba.dbo.datos;
