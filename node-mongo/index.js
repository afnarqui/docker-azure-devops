const express = require('express')
const app = express()
const port = 3000

const MongoClient = require('mongodb').MongoClient

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/test';

app.get('/', (req, res) => {
  MongoClient.connect(mongoUrl, { useNewUrlParser: true }, (err, db) => {
    if (err) {
      res.status(500).send('Error al conectarse con la base de datos ' + err);
    } else {
      res.send('Me conecté a mongodb');
      db.close();
    }
  });
});

app.listen(port, () => console.log(`Server listening on port ${port}!`))
